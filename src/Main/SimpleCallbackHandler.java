package Main;


import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;

public class SimpleCallbackHandler implements CallbackHandler {

	private String username;
	private String password;

	public SimpleCallbackHandler(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	@Override
	public void handle(Callback[] arg0) throws IOException, UnsupportedCallbackException {

		for (Callback callback : arg0) {
			if (callback instanceof NameCallback) {
                            ((NameCallback) callback).setName(this.username);
			}
			else if (callback instanceof PasswordCallback){
				 ((PasswordCallback) callback).setPassword(this.password.toCharArray());
			}
			else {
				throw new UnsupportedCallbackException(callback, "Invalid callback");
			}
		}
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
