package Main;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;



public class SimpleHttpServer {

	public static void main(String[] args) throws Exception {
		HttpServer server = HttpServer.create(new InetSocketAddress(8000), 0);
		HttpContext cp_1 = server.createContext("/test", new Auth());
		HttpContext cp_2 = server.createContext("/handler", new GetHandler());
		server.setExecutor(null);
		server.start();
		System.out.println("The server is running");

	}

	static class Auth implements HttpHandler {
		public void handle(HttpExchange t) throws IOException {

			Headers h = t.getResponseHeaders();
			h.add("Content-Type", "text/html");
			String pathIndex = SimpleHttpServer.class.getResource("/index.html").getPath();
			File file = new File(pathIndex);
			byte[] bytearray = new byte[(int) file.length()];
			FileInputStream fis = new FileInputStream(file);
			BufferedInputStream bis = new BufferedInputStream(fis);
			bis.read(bytearray, 0, bytearray.length);
			SimpleHttpServer.writeResponse(t, file.length(), bytearray);

		}
	}

	static class GetHandler implements HttpHandler {
		public void handle(HttpExchange httpExchange) throws IOException {

			String query = parsePostParameters(httpExchange);
			Map<String, String> params = queryToMap(query);
			
			String str = "Invalid password";
			
			String jassConfig = SimpleHttpServer.class.getResource("/jaas.config").getPath();

			System.setProperty("java.security.auth.login.config", jassConfig);

			SimpleCallbackHandler exampleCallBackHandler = new SimpleCallbackHandler(params.get("username"), params.get("password"));

			LoginContext loginContext;
			try {
				loginContext = new LoginContext("ExLogin", exampleCallBackHandler);
				loginContext.login();
				System.out.println("Login successful");
				str = "Login successful";
				
			} catch (LoginException e) {

				System.out.println("Invalid login or password");
			}


			httpExchange.sendResponseHeaders(200, str.length());
			OutputStream os = httpExchange.getResponseBody();
			os.write(str.getBytes());
			os.close();
		}
	}

	public static void writeResponse(HttpExchange t, long fileLenght, byte[] bytes) throws IOException {

		t.sendResponseHeaders(200, fileLenght);
		OutputStream os = t.getResponseBody();
		os.write(bytes, 0, bytes.length);
		os.close();
	}

	public static Map<String, String> queryToMap(String query) {
		Map<String, String> result = new HashMap<String, String>();
		for (String param : query.split("&")) {
			String pair[] = param.split("=");
			if (pair.length > 1) {
				result.put(pair[0], pair[1]);
			} else {
				result.put(pair[0], "");
			}
		}
		return result;
	}

	public static String parsePostParameters(HttpExchange exchange) throws IOException {
		String query = null;
		if ("post".equalsIgnoreCase(exchange.getRequestMethod())) {
			@SuppressWarnings("unchecked")
			Map parameters = (Map) exchange.getAttribute("parameters");
			InputStreamReader isr = new InputStreamReader(exchange.getRequestBody(), "utf-8");
			BufferedReader br = new BufferedReader(isr);
			query = br.readLine();

		}
		return query;
	}

}