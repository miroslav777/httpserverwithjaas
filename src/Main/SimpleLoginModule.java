package Main;


import java.io.IOException;
import java.util.Map;

import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;

public class SimpleLoginModule implements LoginModule{

	private Subject subject;
	private CallbackHandler callbackHandler;
	
	@Override
	public boolean abort() throws LoginException {
				return true;
	}

	@Override
	public boolean commit() throws LoginException {
		
		return true;
	}

	@Override
	public void initialize(Subject arg0, CallbackHandler arg1,
			Map<String, ?> arg2, Map<String, ?> arg3) {
		
		this.subject = arg0;
		this.callbackHandler = arg1;
	}

	@Override
	public boolean login() throws LoginException {
		
		boolean isLoggedin = false;
		
		if (this.callbackHandler == null){
			throw new LoginException("No callback handler");
		} 
		
		Callback[] callbacks = new Callback[2];
		callbacks[0] = new NameCallback("username");
		callbacks[1] = new PasswordCallback("password", false);
		
		try {
			this.callbackHandler.handle(callbacks);
			
			String username = ((NameCallback)callbacks[0]).getName();
			String password = new String(((PasswordCallback)callbacks[1]).getPassword());
		
		    if (username.equals("admin") && password.equals("admin")){
		    	isLoggedin = true;
		    }
		   
		} catch (IOException e) {
		
			e.printStackTrace();
		} catch (UnsupportedCallbackException e) {
			
			e.printStackTrace();
		}
		
		return isLoggedin;
	}

	@Override
	public boolean logout() throws LoginException {
		
		return true;
	}

}
